# Spring-boot helloworld

## Objetivo

Aplicación sencilla Java desarrollada con Springboot. Es un microservicio que cuando
levanta se pone escuchar en el puerto 8080.

Con este ejemplo puedo ver:
 * Como es una aplicación Springboot
 * Como construirla con maven
 * Integrarla con github actions
 * Almacenar artefactos en packages
 * Construcción Imagen Inmutable multiplataforma


## Publicación artefacto maven en package

  * Importante tener un personal Access Token con permisos sobre package.

  * Setting.xml personalizado añadiendo password en sección server y publicamos repositorios.

``` xml
<profiles>
  <profile>
    <id>github</id>
    <repositories>
      <repository>
        <id>github</id>
        <name>GitHub OWNER Apache Maven Packages</name>
        <url>https://maven.pkg.github.com/jmmirand-kube-example/springboot-app</url>
      </repository>
    </repositories>
  </profile>
</profiles>

<servers>
  <server>
    <id>github</id>
    <username>jmmirand</username>
    <password>5937be6dfd88684a3f8cad0928b73c4d685e7569</password>
    <password>${env.GITHUBTOKEN}</password>
  </server>
</servers>
</settings>
```

  * Añadir en el pom.xml donde desplegamos.

``` xml
  <distributionManagement>
   <repository>
     <id>github</id>
     <name>GitHub OWNER Apache Maven Packages</name>
     <url>https://maven.pkg.github.com/jmmirand-kube-example/springboot-app</url>
   </repository>
	</distributionManagement>
```





* **referencias**
  - [configuring-apache-maven-for-use-with-github-packages](https://docs.github.com/es/free-pro-team@latest/packages/using-github-packages-with-your-projects-ecosystem/configuring-apache-maven-for-use-with-github-packages)
  - [maven distribution-management](https://maven.apache.org/pom.html#distribution-management)
