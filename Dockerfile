FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ./initial/target/spring-boot-helloword.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
